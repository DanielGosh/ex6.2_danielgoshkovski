#pragma once
#include <exception>

class shapeException : public std::exception
{
	virtual const char* what() const throw()
	{
		return "This is a shape exception!";
	}
};