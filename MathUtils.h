#pragma once
#include <math.h>
class MathUtils
{
public:
	int CallPentagonArea(double len);
	int CallHexagonArea(double len);
};

