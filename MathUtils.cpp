#include "MathUtils.h"

int MathUtils::CallPentagonArea(double len)
{
	return 0.25 * (sqrt(5 * (5 + 2 * sqrt(5)))) * (len * len);
}

int MathUtils::CallHexagonArea(double len)
{
	return ((3 * sqrt(3))/2) * (len * len);
}
